---- State ----
status: READY
 -- program counter --
x50 = thread join (x47)
-- Memory:
-- Global objects:
mo1 ('v', alloc'd by g1), global, size: 1:32b
  0 -> 50:8b
-- Global bindings:
g1 -> ptr(1:32b, 0:32b)
-- Objects:
mo2 ('<unnamed>', alloc'd by x31), size: 4:32b
  0 -> 0:32b
mo3 ('<unnamed>', alloc'd by k), size: 1:32b
  0 -> 109:8b
mo4 ('<unnamed>', alloc'd by t1), size: 4:32b
  0 -> 1:32b
mo5 ('<unnamed>', alloc'd by t2), size: 4:32b
  0 -> 2:32b
-- Call stack:
 -- 0: main --
x31 -> ptr(2:32b, 0:32b)
k -> ptr(3:32b, 0:32b)
t1 -> ptr(4:32b, 0:32b)
t2 -> ptr(5:32b, 0:32b)
x37 -> 109:8b
x40 -> <__thread_create_succeeded:32b>
x41 -> 1:32b
x44 -> <__thread_create_succeeded#2:32b>
x45 -> 2:32b
x49 -> <__join_succeeded#6:32b>
scoped objects: mo2, mo3, mo4, mo5
 -- nondets --
x40 = <__thread_create_succeeded:32b>
x44 = <__thread_create_succeeded#2:32b>
x49 = <__join_succeeded#6:32b>
 -- constraints --
[]
 -- Threads --
  0: 0
 -- Exited threads waiting for join: {1: ptr(0:32b, 0:32b), 2: ptr(0:32b, 0:32b)}
 -- Threads waiting in join: {}
 -- Mutexes (locked by): {}
 -- Threads waiting for mutexes: {}
 -- Events --
